import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Pickers 1.3
import Ubuntu.Components.Popups 1.3

ListItem {
    id: root

    property string text: ""
    property var date: new Date();
    property var minimum: new Date();

    property var __invalidDate: new Date(NaN)

    ListItemLayout {
        id: layout
        title.text: root.text
        subtitle.text: Qt.formatDateTime(date, Qt.DefaultLocaleLongDate)
        ProgressionSlot {}
    }

    onClicked: {
        PopupUtils.open(dialogComponent, root, { "date": date })
    }

    Component {
        id: dialogComponent

        Dialog {
            id: dialog

            title: i18n.tr("Enter a date")
            property var date: new Date()

            DatePicker {
                id: datePicker
                anchors.left: parent.left
                anchors.right: parent.right
                minimum: root.minimum
                date: dialog.date
            }

            DatePicker {
                id: timePicker
                mode: "Hours|Minutes"
                minimum: root.minimum
                date: dialog.date
            }

            Button {
                text: i18n.tr("Done")
                onClicked: {
                    var day = datePicker.date
                    var time = timePicker.date
                    var chosenDate =
                        new Date(Date.UTC(day.getUTCFullYear(),
                                          day.getUTCMonth(),
                                          day.getUTCDate(),
                                          time.getUTCHours(),
                                          time.getUTCMinutes()))

                    root.date = chosenDate
                    PopupUtils.close(dialog)
                }
            }
        }
    }
}
