import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.Components.Pickers 1.3
import Ubuntu.OnlineAccounts 2.0

Page {
    id: root

    property var livestream: null
    property var account: null

    signal done()

    property bool _busy: true
    property string _apiUrl: "https://www.googleapis.com/youtube/v3/"
    property var _auth: null
    property string _errorMessage: ""
    property string _streamId: ""
    property string _broadcastId: ""
    property string _ingestionUrl: ""
    property var _privacyModel: [ "public", "private", "unlisted" ]

    header: PageHeader {
        title: i18n.tr("Setup YouTube livestream")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: root.saveAndClose()
            }
        ]
    }

    Component.onCompleted: account.authenticate({})

    Connections {
        id: accountConnection
        target: root.account
        onAuthenticationReply: {
            root._busy = false
            var reply = authenticationData
            console.log("Reply: " + JSON.stringify(reply))
            if ("errorCode" in reply) {
                console.warn("Authentication error: " + reply.errorText + " (" + reply.errorCode + ")")
            } else {
                root._auth = authenticationData
            }
        }
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }
            enabled: !_busy

            Label {
                Layout.fillWidth: true
                text: i18n.tr("An error occurred:\n%1").arg(_errorMessage)
                visible: _errorMessage.length > 0
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                color: "red"
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Broadcast name:")
            }

            TextField {
                id: broadcastNameField
                Layout.fillWidth: true
            }

            OptionSelector {
                id: privacyControl
                Layout.fillWidth: true
                property var _labels: {
                    "public": i18n.tr("Public"),
                    "private": i18n.tr("Private"),
                    "unlisted": i18n.tr("Unlisted")
                }
                expanded: false
                text: i18n.tr("Visibility")
                model: _privacyModel
                delegate: OptionSelectorDelegate {
                    text: privacyControl._labels[modelData]
                }
            }

            DateTimeChooser {
                id: startTimeControl
                Layout.fillWidth: true
                text: i18n.tr("Estimated start time")
            }

            Button {
                id: startButton
                anchors {
                    left: parent.left; right: parent.right
                    margins: units.gu(4)
                }
                text: i18n.tr("Create livestream")
                enabled: root.configurationIsValid()
                color: theme.palette.normal.positive
                onClicked: {
                    root._errorMessage = ""
                    root._busy = true;
                    insertStream()
                }
            }
        }
    }

    ActivityIndicator {
        anchors.centerIn: parent
        running: _busy
    }

    function configurationIsValid() {
        return broadcastNameField.text.length > 0
    }

    function postRequest(url, body, callback) {
        var http = new XMLHttpRequest()
        var requestUrl = _apiUrl + url;
        http.open("POST", requestUrl, true);
        http.setRequestHeader("Authorization", "Bearer " + _auth.AccessToken)
        http.setRequestHeader("Accept", "application/json")
        http.setRequestHeader("Content-Type", "application/json")
        http.onreadystatechange = function() {
            if (http.readyState === XMLHttpRequest.DONE) {
                callback(http)
            }
        }
        http.send(JSON.stringify(body));
    }

    function insertStream() {
        var name = "stream1"

        var callback = function(xhr) {
            console.log("response text: " + xhr.responseText)
            var response = JSON.parse(xhr.responseText)
            if (xhr.status == 200) {
                console.log("ok")
                root._streamId = response.id
                var ingestionInfo = response.cdn.ingestionInfo
                root._ingestionUrl = ingestionInfo.ingestionAddress +
                    '/' + ingestionInfo.streamName
                insertBroadcast()
            } else {
                reportError(response)
            }
        }
        var body = {
            "snippet": { "title": name },
            "cdn": {
                "resolution": "variable",
                "frameRate": "variable",
                "ingestionType": "rtmp"
            },
            "contentDetails": {
                "isReusable": true
            }
        }
        postRequest("liveStreams?part=snippet,cdn,contentDetails",
                    body, callback)
    }

    function insertBroadcast() {
        var name = broadcastNameField.text
        var startTime = startTimeControl.date.toISOString()
        var privacyStatus = _privacyModel[privacyControl.selectedIndex]

        var callback = function(xhr) {
            console.log("response text: " + xhr.responseText)
            var response = JSON.parse(xhr.responseText)
            if (xhr.status == 200) {
                console.log("ok")
                root._broadcastId = response.id
                bindBroadcast()
            } else {
                reportError(response)
            }
        }
        var body = {
            "snippet": {
                "title": name,
                "scheduledStartTime": startTime
            },
            "status": {
                "privacyStatus": privacyStatus
            }
        }
        postRequest("liveBroadcasts?part=snippet,status",
                    body, callback)
    }

    function bindBroadcast() {
        var callback = function(xhr) {
            console.log("response text: " + xhr.responseText)
            var response = JSON.parse(xhr.responseText)
            if (xhr.status == 200) {
                console.log("ok")
                root._busy = false
                saveAndClose()
            } else {
                reportError(response)
            }
        }
        postRequest("liveBroadcasts/bind?id=" + _broadcastId +
                    "&streamId=" + _streamId +
                    "&part=id,contentDetails",
                    '', callback)
    }

    function reportError(response) {
        _errorMessage = response.error.message
        _busy = false
    }

    function saveAndClose() {
        if (broadcastNameField.text.length > 0) {
            livestream.name =
                i18n.tr("YouTube - %1").arg(broadcastNameField.text)
        } else {
            livestream.name = i18n.tr("YouTube")
        }
        livestream.url = _ingestionUrl
        done()
        pageStack.pop()
    }
}
