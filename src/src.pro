include(../common-config.pri)

TEMPLATE = app
TARGET = yi-livestream

QT += \
    qml \
    quick

CONFIG += \
    c++11 \
    link_pkgconfig

PKGCONFIG += \
    libqrencode

SOURCES += \
    main.cpp \
    qrcodegenerator.cpp \
    qrcodeimageprovider.cpp

HEADERS += \
    qrcodegenerator.h \
    qrcodeimageprovider.h

RESOURCES += \
    app.qrc

EMBED_LIBS = qrencode
EMBED_LIBDIR = "$$[QT_HOST_LIBS]"

for (lib, EMBED_LIBS) {
    so_file = lib$${lib}.so
    extra_libs.files += $$files($${EMBED_LIBDIR}/$${so_file}.*)
}
extra_libs.path = $${INSTALL_LIB_DIR}
INSTALLS += extra_libs

DEFINES += \
    APPLICATION_NAME=\\\"$${APPLICATION_NAME}\\\"

target.path = $${INSTALL_PREFIX}/bin
INSTALLS+=target
