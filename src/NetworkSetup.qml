import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var configuration: null

    signal done()

    header: PageHeader {
        title: i18n.tr("Network access point")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: root.saveAndClose()
            }
        ]
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Access point name:")
            }

            TextField {
                id: ssidField
                Layout.fillWidth: true
                text: root.configuration.ssid
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
                Keys.onReturnPressed: pwdField.forceActiveFocus()
            }

            Label {
                Layout.topMargin: units.gu(1)
                Layout.fillWidth: true
                text: i18n.tr("Access point password:")
            }

            TextField {
                id: pwdField
                Layout.fillWidth: true
                text: root.configuration.pwd
                echoMode: TextInput.PasswordEchoOnEdit
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
                Keys.onReturnPressed: root.saveAndClose()
            }
        }
    }

    function saveAndClose() {
        configuration.ssid = ssidField.text
        configuration.pwd = pwdField.text
        done()
        pageStack.pop()
    }
}
