import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var configuration: null
    property var rates: null
    property var _ratesModel: [ "0", "1", "2", "3" ]

    signal done()

    header: PageHeader {
        title: i18n.tr("Select video rate")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: root.saveAndClose()
            }
        ]
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            ListItem.ItemSelector {
                id: control
                Layout.fillWidth: true
                Layout.fillHeight: true
                expanded: true
                selectedIndex: _ratesModel.indexOf(configuration.res)
                model: _ratesModel
                delegate: OptionSelectorDelegate {
                    text: rates[modelData]
                }
                onDelegateClicked: {
                    selectedIndex = index
                    root.saveAndClose()
                }
            }
        }
    }

    function saveAndClose() {
        configuration.rate = _ratesModel[control.selectedIndex]
        done()
        pageStack.pop()
    }
}
