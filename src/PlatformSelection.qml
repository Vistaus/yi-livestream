import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.OnlineAccounts 2.0

Page {
    id: root

    property var livestream: null
    property var broadcastsModel: null

    signal done()

    property var _providers: [
        {
            "name": i18n.tr("Add YouTube account"),
            "serviceId": "it.mardy.yi-livestream_yi-livestream_google",
        }
    ]

    property var _servicePages: {
        "it.mardy.yi-livestream_yi-livestream_google": "YouTube.qml",
    }

    header: PageHeader {
        title: i18n.tr("Select live platform")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: root.saveAndClose()
            }
        ]
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            Label {
                Layout.fillWidth: true
                Layout.topMargin: units.gu(2)
                Layout.leftMargin: units.gu(1)
                text: i18n.tr("Resume a previous broadcast")
                font.bold: true
                visible: savedBroadcastsRepeater.count > 0
            }

            Repeater {
                id: savedBroadcastsRepeater
                model: broadcastsModel

                ListItem {
                    Layout.fillWidth: true
                    height: units.gu(5)
                    leadingActions: ListItemActions {
                        actions: [
                            Action {
                                iconName: "delete"
                                onTriggered: root.deleteBroadcast(index)
                            }
                        ]
                    }
                    ListItemLayout {
                        title.text: modelData.name
                    }
                    onClicked: {
                        livestream = modelData
                        root.saveAndClose()
                    }
                }
            }

            Label {
                Layout.fillWidth: true
                Layout.topMargin: units.gu(2)
                Layout.leftMargin: units.gu(1)
                text: i18n.tr("Create a new broadcast")
                font.bold: true
            }

            Repeater {
                model: accounts

                ListItem {
                    Layout.fillWidth: true
                    height: units.gu(7)
                    ListItemLayout {
                        title.text: model.service.displayName
                        subtitle.text: model.displayName
                        ProgressionSlot {}
                    }
                    onClicked: {
                        var pageFile = _servicePages[model.serviceId]
                        var page = pageStack.push(Qt.resolvedUrl(pageFile), {
                            "account": model.account,
                            "livestream": livestream,
                        })
                        page.onDone.connect(function() {
                            livestream = page.livestream
                            root.saveAndClose()
                        })
                    }
                }
            }

            Repeater {
                model: _providers

                ListItem {
                    Layout.fillWidth: true
                    ListItemLayout {
                        title.text: modelData.name
                        ProgressionSlot {}
                    }
                    onClicked: accounts.requestAccess(modelData.serviceId, {})
                }
            }

            ListItem {
                Layout.fillWidth: true
                ListItemLayout {
                    title.text: i18n.tr("Custom livestream")
                    subtitle.text: livestream.accountName == "custom" ? livestream.name : ""
                    ProgressionSlot {}
                }
                onClicked: {
                    var page = pageStack.push(Qt.resolvedUrl("CustomLivestream.qml"), {
                        "livestream": _livestream,
                    })
                    page.onDone.connect(function() {
                        livestream = page.livestream
                        root.saveAndClose()
                    })
                }
            }
        }
    }

    AccountModel {
        id: accounts
        applicationId: "it.mardy.yi-livestream_yi-livestream"
    }

    function deleteBroadcast(index) {
        /* if this is the currently selected broadcast, reset the livestream
         * variable */
        if (livestream.url == broadcastsModel[index].url) {
            livestream = { "name": "", "url": "" }
        }

        var newBroadcastsModel = broadcastsModel
        newBroadcastsModel.splice(index, 1)
        broadcastsModel = newBroadcastsModel
    }

    function saveAndClose() {
        done()
        pageStack.pop()
    }
}
