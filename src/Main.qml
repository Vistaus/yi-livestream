/*
 * Copyright (C) 2018 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Qt.labs.settings 1.0
import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 1.3
import YI 0.1

MainView {
    id: root
    applicationName: "it.mardy.yi-livestream"
    automaticOrientation: true

    width: units.gu(40)
    height: units.gu(75)

    property var _defaultConfiguration: {
        "ssid": "",
        "pwd": "",
        "res": "720p",
        "rate": "0",
        "dur": "0",
        "ak": "0",
        "sign": "",
    }
    property var _configuration: null
    property var _broadcastsModel: []
    property var _livestream: {
        "name": "",
        "url": "",
    }
    property var _subPage: null
    property var _resolutions: {
        "1080p": i18n.tr("FHD 1080p"),
        "720p": i18n.tr("HD 720p"),
        "480p": i18n.tr("SD 480p")
    }
    property var _rates: {
        "0": i18n.tr("Auto"),
        "1": i18n.tr("Low (0.6 Mbps)"),
        "2": i18n.tr("Middle (0.8 Mbps)"),
        "3": i18n.tr("High (1.2 Mbps)")
    }

    Component.onCompleted: {
        try {
            root._configuration = JSON.parse(settings.configuration)
        } catch (e) {
            root._configuration = root._defaultConfiguration
        }

        try {
            root._broadcastsModel = JSON.parse(settings.broadcasts)
        } catch (e) {}
    }

    PageStack {
        id: pageStack

        Component.onCompleted: pageStack.push(mainPage)

        Page {
            id: mainPage
            visible: false

            header: PageHeader {
                title: i18n.tr("YI Livestream")
                flickable: flick
            }

            Connections {
                target: root._subPage
                onDone: _configuration = root._subPage.configuration
            }

            Flickable {
                id: flick
                anchors.fill: parent
                contentHeight: contentItem.childrenRect.height
                bottomMargin: startButtonArea.height

                ColumnLayout {
                    anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

                    ListItem.Subtitled {
                        Layout.fillWidth: true
                        text: i18n.tr("Network setup")
                        subText: _configuration.ssid
                        progression: true
                        onClicked: {
                            root._subPage = pageStack.push(Qt.resolvedUrl("NetworkSetup.qml"), {
                                "configuration": _configuration,
                            })
                        }
                    }

                    ListItem.Subtitled {
                        Layout.fillWidth: true
                        text: i18n.tr("Video resolution")
                        subText: _resolutions[_configuration.res]
                        progression: true
                        onClicked: {
                            root._subPage = pageStack.push(Qt.resolvedUrl("Resolution.qml"), {
                                "configuration": _configuration,
                                "resolutions": _resolutions,
                            })
                        }
                    }

                    ListItem.Subtitled {
                        Layout.fillWidth: true
                        text: i18n.tr("Bit rate")
                        subText: _rates[_configuration.rate]
                        progression: true
                        onClicked: {
                            root._subPage = pageStack.push(Qt.resolvedUrl("Rate.qml"), {
                                "configuration": _configuration,
                                "rates": _rates,
                            })
                        }
                    }

                    ListItem.Subtitled {
                        Layout.fillWidth: true
                        text: i18n.tr("Live platform")
                        subText: _livestream.name
                        progression: true
                        onClicked: {
                            var page = pageStack.push(Qt.resolvedUrl("PlatformSelection.qml"), {
                                "livestream": _livestream,
                                "broadcastsModel": _broadcastsModel,
                            })
                            page.onDone.connect(function() {
                                _broadcastsModel = page.broadcastsModel
                                _livestream = page.livestream
                                saveBroadcast(page.livestream)
                            })
                        }
                    }
                }
            }

            Rectangle {
                id: startButtonArea
                anchors {
                    left: parent.left; right: parent.right;
                    bottom: flick.bottom
                }
                height: startButton.height + units.gu(2)
                color: theme.palette.normal.background

                Button {
                    id: startButton
                    anchors {
                        left: parent.left; right: parent.right
                        margins: units.gu(4)
                        verticalCenter: parent.verticalCenter
                    }
                    text: i18n.tr("Start")
                    enabled: root.configurationIsValid()
                    color: theme.palette.normal.positive
                    onClicked: pageStack.push(Qt.resolvedUrl("QRPage.qml"), {
                            "configuration": _configuration,
                            "livestream": _livestream,
                        })
                }
            }
        }
    }

    Settings {
        id: settings
        property string configuration: JSON.stringify(root._defaultConfiguration)
        property string broadcasts: JSON.stringify(root._broadcastsModel)
    }

    Binding {
        target: settings
        property: "configuration"
        value: JSON.stringify(root._configuration)
        when: root._configuration != null
    }

    function configurationIsValid() {
        return _configuration.ssid.length > 0 &&
            _configuration.pwd.length > 0 &&
            _livestream.url.length > 0
    }

    function saveBroadcast(broadcast) {
        if (broadcast.url.length == 0) return
        for (var i = 0; i < _broadcastsModel.length; i++) {
            var b = _broadcastsModel[i]
            if (b.url == broadcast.url) return
        }

        var newBroadcastsModel = _broadcastsModel
        newBroadcastsModel.push(broadcast)
        _broadcastsModel = newBroadcastsModel;
    }
}

